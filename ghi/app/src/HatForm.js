import React, { useEffect, useState} from 'react';

function HatForm(props) {

    const [locations, setLocations] = useState([]);
    async function loadLocations() {
        const response = await fetch('http://localhost:8100/api/locations/')
        if(response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
        else{
            console.error(response)
        }
    }
    useEffect(() => {
        loadLocations()
    }, []);

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data ={};
        data.style_name = name;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = picture;
        data.location = location;
        console.log(data)

        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok){
            const data = await response.json();
            console.log(data)
            setName('');
            setFabric('');
            setColor('');
            setPicture('');
            setLocation('');
        }
        // window.location.reload();

    }
    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create Hat</h1>
                <form onSubmit={handleSubmit} className="row g-3" id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricChange} placeholder="fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureChange} placeholder="URL Here" required type="text" value={picture} name="picture" id="picture" className="form-control"/>
                        <label htmlFor="picture">Picture URL</label>
                    </div>
                    <div className="col-md-4">
                        <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
                        <option value=''>Choose Closet</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    // <form onSubmit={handleSubmit} className="row g-3" id="create-hat-form">
    //     <li>
    //         <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control"/>
    //     </li>
        
    //     <li>
    //         <input onChange={handleFabricChange} placeholder="fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control"/>
    //     </li>
    //     <li>
    //         <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control"/>

    //     </li>
    //     <li>
    //         <input onChange={handlePictureChange} placeholder="URL Here" required type="text" value={picture} name="picture" id="picture" className="form-control"/>

    //     </li>
    //     <li>
    //         <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
    //         <option value=''>Choose Closet</option>
    //         {locations.map(location => {
    //             return (
    //                 <option key={location.href} value={location.href}>
    //                     {location.closet_name}
    //                 </option>
    //             )
    //         })}
    //         </select>
    //     </li>
    //     <button className="btn btn-primary">Create</button>
    // </form>
    );
}

export default HatForm;