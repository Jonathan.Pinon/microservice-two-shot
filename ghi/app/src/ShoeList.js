import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function ListShoeForm() {
    const [shoeList, setShoeList] = useState([]);

    useEffect(() => {
        loadShoes();
    }, []);

    async function deleteShoe(id)
    {
        const fetchConfig = {
            method: "delete"
        };
        const response = await fetch('http://localhost:8080/shoes/api/'+id+'/delete', fetchConfig);
        if(response.ok) {
            loadShoes();
        }
    }

    async function loadShoes() {
        const response = await fetch('http://localhost:8080/shoes/api/');
        if(response.ok) {
            const data = await response.json();
            setShoeList(data.shoes);
        }
        else {
            console.error(response);
        }
    }

    const funremoveShoe=((id)=>{
        if(window.confirm("Do you want to delete?")){
            fetch('http://localhost:8080/shoes/api/'+id+'/delete',
            {method:"DELETE"}).then(()=>{
                //window.location.reload();
            }).catch((err)=>{
                console.log(err);
            })
        }
        })


return (
    <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to='/shoeslist/new' className="btn-btn-primary btn-lg px-4 gp-3"></Link>
        </div>
        <br />
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
        </tr>
        </thead>
    <tbody>
        {shoeList.map(shoe =>
            <tr key={ shoe.m_name }>
                <td>{ shoe.m_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td><img src={ shoe.picture_url } alt={ shoe.m_name } width="200" height="200"/></td>
                <td>{ shoe.bin.bin_number }</td>
                <td><button className="btn btn-danger" onClick={() => {deleteShoe(shoe.id)}}>Delete</button></td>
            </tr>
        ) }
    </tbody>
    </table>
    </div>
    );
}
export default ListShoeForm;
