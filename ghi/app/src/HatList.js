import React, { useEffect, useState }  from 'react';
// import { NavLink } from 'react-router-dom';
// import { Link } from 'react-router-dom';
import HatForm from './HatForm';


function HatList() {
    const [hatList, setHatList] = useState([])
    async function loadHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if(response.ok) {
            const data = await response.json();
            setHatList(data.hats);
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadHats()
    }, []);
    
    async function deleteHat(href){
        const fetchConfig = {
            method: "delete"
        };
        const response = await fetch(`http://localhost:8090${href}`, fetchConfig);
        if(response.ok) {
            loadHats();
        }
    }
    return (
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
        {/* //     <Link to='/hatList/new' className="btn-btn-primary btn-lg px-4 gp-3"></Link> */}
            <table className="table table-striped">
                <thead>
                    {/* <tr>
                        <div className="dropdown">
                            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Create Hat
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            {<HatForm></HatForm>}
                            </div>
                        </div>
                    </tr> */}
                    <tr>
                        <th>Name</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Closet</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {hatList.map((hat) => {
                        return (
                            <tr key={hat.href}>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.color }</td>
                                <td>{ hat.location}</td>
                                <td>
                                    <img src={ hat.picture_url } height="100" width="100"/>
                                </td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => {deleteHat(hat.href)}}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default HatList;



