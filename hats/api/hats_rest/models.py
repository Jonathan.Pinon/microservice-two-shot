from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"
    
    class Meta:
        ordering = ["closet_name", "section_number", "shelf_number"]

class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="wardrobe",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
    
    class Meta:
        ordering = ("style_name", )
    
    def get_api_url(self):
        return reverse("show_hat", kwargs={"pk": self.pk})