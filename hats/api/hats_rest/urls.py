from django.urls import path
from .views import hat_list, show_hat

urlpatterns = [
    path('hats/', hat_list, name="create_hats"),
    # path('location/<int:pk>/hats/', hat_list, name="create_hats"),
    path('hats/<int:pk>/', show_hat, name="show_hat"),
    # path('hats/<int:pk>/delete', show_hat, name="delete_hat")
]
