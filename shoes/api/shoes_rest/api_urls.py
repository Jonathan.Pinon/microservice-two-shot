from django.urls import path
from shoes_rest.views import get_shoe_list, create_new_shoe, delete_shoe

urlpatterns = [
    path('', get_shoe_list, name="get_shoe_list"),
    path("create", create_new_shoe, name="create_new_shoe"),
    path("<int:id>/delete", delete_shoe, name="delete_shoe"),
]
