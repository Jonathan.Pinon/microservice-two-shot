from django.http import JsonResponse
from common.json import ModelEncoder
from .models import BinV0, Shoe
from django.views.decorators.http import require_http_methods
import json


class BinEncoder(ModelEncoder):
    model = BinV0
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "id",
        "import_href"
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "color",
        "picture_url",
        "m_name",
        "bin",
        "id"
    ]
    encoders = {
        "bin": BinEncoder(),
    }


# Create your views here.


@require_http_methods(["GET"])
def get_shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"message": "Invalid Method"},
            status=400,
        )


@require_http_methods(["POST"])
def create_new_shoe(request):
    if request.method == "POST":
        content = json.loads(request.body)
        print(content)
        shoe = BinV0.objects.filter(id=content["bin"])
        #shoe = Shoe.objects.create(**content)
        shoe = Shoe(
            manufacturer=content["manufacturer"],
            m_name=content["m_name"],
            color=content["color"],
            picture_url=content["picture_url"],
            bin=BinV0.objects.get(id=content["bin"])
        )
        shoe.save()
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"message": "Invalid Method"},
            status=400,
        )


@require_http_methods(["DELETE"])
def delete_shoe(request, id):
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse({"message": "Shoe deleted successfully"},
                                status=204,
                                )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe not found"},
                status=404,
            )
