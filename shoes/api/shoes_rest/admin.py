from django.contrib import admin
from .models import LocationV0, BinV0, Shoe
# Register your models here.
admin.site.register(LocationV0)
admin.site.register(BinV0)
admin.site.register(Shoe)
