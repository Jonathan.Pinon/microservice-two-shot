# Wardrobify

Team:

* Person 1 - Which microservice?
Jonathan hats microservice
* Person 2 - Which microservice?
Preeti shoes microservice
* Jonathan Pinon - Hats microservice
* Preeti Mahar - Shoes microservice

## Design
![Design](/docs/Diagram.png)
## Usage
The React App would be accessed via http://localhost:3000 .
## Setup

steps: After being given the 'maintainer' status by my partner who forked
microservice-two-shot from the gitLab.I did :-

git clone [repository url]
cd microservice-two-shot
docker-compose build
docker-compose up

## Setup

steps: After being given the 'maintainer' status by my partner who forked
microservice-two-shot from the gitLab.I did :-

git clone [repository url]
cd microservice-two-shot
docker-compose build
docker-compose up

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

    This micro-service is based on the 2 models:
         BinV0 and Shoe respectively in the shoes_rest/models.py.
    python manage.py makemigrations // Typed at the container command prompt to make sure that it generates the required migrations.
    python manage.py migrate // to apply the migration.
    The views are made in the shoes_rest/views.py
    and their urls(paths) in the shoes_rest/api_urls.py.
    Their are 3 views namely, get_shoe_list, create_new_shoe, delete_shoe
     for generating a shoe List, creating a new shoe and deleting a shoe respectively!

     The 2 forms associated with them which go into the front-end are in the :-ghi/app/src , namely:
     ShoeForm.js and ShoeList.js to create a new shoe and generate a List of shoes or to delete a particular one from the database respectively!

     Shoes microservices can be accessed using port 8080.

    * After making changes to the views and url files, needed to build the containers again and rerun everything by:-

    * docker container prune -f // to delete all containers

    * docker-compose build  // to build all images
    * docker-compose up    // to run the containers

    * Did - git add .
    * git commit - m "text"
    * git push
    * git checkout shoes-branch
    * git pull
    * git push
    * git merge main
    * git checkout main
    * git pull
    * git merge shoes-branch
    * git push
    from time to time to add changes to the gitlab. Later resolved merge conflicts, saved the files and pushed changes to the origin/main.

    The shoes model has following views.

|   Action  |  Method  |   URL  |
|---|---|---|
|  List shoes   |  GET |     http://localhost:8080/shoes/api     |
| Create a shoe | POST | http://localhost:8080/shoes/api/create  |

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I made two models, LocationVO and Hat, each containing object requirements for the creation of hats in wardrobify. Then, I created three model encoders (HatDetail, LocationVO and HatList) that used the model encoder to convert my data, and two view functions with GET (to get the list of hats) and POST (creating a new hat and adding to the list) methods. After the models and their associated view functions, I created URL paths and used React to develop a HatList JS file that fetched data from the URL path and equivalent routes in App JS, displayed it in a table, and included a delete button to remove any unwanted items. I also created a HatForm JS file that included a fetch request to the locations API to store the new hat data to its own location, and a handle submit event.
