from django.contrib import admin

# Register your models here.
from .models import Location, Bin


admin.site.register(Location)
admin.site.register(Bin)
